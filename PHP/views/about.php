<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Covaware</title>
  <link rel="stylesheet" href="./../css/About.css">
</head>
<body>
  <header>
    <div>
      <img src="./../Images/Logo.svg" id = "logo" alt="logo" style="width: 141.74px; height: 38.11px; align-self: center;">
      <p id = "header-button" onclick="javascript:window.location='./../index.php'">Home</p>
    </div>
  </header>
  <main>
    <div id = "main-div">
      <img src="./../Images/Philippines - Background.svg" alt = "background" style="position: absolute; z-index: -1; top: -500px; transform: rotate(-45deg);">
    </div>
    <div id = "about-div">
      <div id = "motto">
        <p id = group>Covaware</p>
        <p id = "message">For the safety of the filipino people.</p>
      </div>
      <div id = "team">
        <p id = group>Team</p>
        <p id = "name">Paulo Evangelista</p>
        <p id = "name">Luigi Gandeza</p>
        <p id = "name">Abrience Liao</p>
        <p id = "name">Calee Jan Mangunay</p>
        <p id = "name">Chris Trajano</p>
      </div>
      <div id = "team">
        <p id = group>SD 12C</p>
        <img src="Images/Flag.svg" style="height:40.85px;width:81.7px;">
      </div>
    </div>
  </main>
</body>
</html>
