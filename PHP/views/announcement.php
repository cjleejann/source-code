<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>About</title>
  <link rel="stylesheet" href="./../css/Announcement.css">
</head>
<body>
  <header>
    <div>
      <img src="./../Images/Logo.svg" id = "logo" alt="logo" style="width: 141.74px; height: 38.11px; align-self: center;">
      <p id = "header-button" onclick="javascript:window.location='./../index.php'">Home</p>
    </div>
  </header>
  <main>
    <img src="./../Images/Philippines - Background.svg" alt = "background" style="position: absolute; z-index: -1; top: -500px; transform: rotate(-45deg);">
    <div id = "about-div">
      <div id = "motto">
        <p id = title style="font-size: 45px;">Covaware</p>
        <p id = publisher style="font-size: 25px; margin-top: -50px;">Malacañang - April 29, 2020</p>
        <p id = "message">
          Clean your hands often. Use soap and water, or an alcohol-based hand rub.
          Maintain a safe distance from anyone who is coughing or sneezing.
          Don’t touch your eyes, nose or mouth.
          Cover your nose and mouth with your bent elbow or a tissue when you cough or sneeze.
          Stay home if you feel unwell.
          If you have a fever, a cough, and difficulty breathing, seek medical attention. Call in advance.
          Follow the directions of your local health authority.
        </p>
      </div>
    </div>
  </main>
</body>
</html>
