<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Covaware</title>
  <link rel="stylesheet" href="./../css/Covid-19.css">
</head>
<body>
  <header>
    <div>
      <img src="../Images/Logo.svg" id = "logo" alt="logo" style="width: 141.74px; height: 38.11px; align-self: center;">
      <p id = "header-button" onclick="javascript:window.location='./../index.php'">Home</p>
    </div>
  </header>
  <main>
    <div id = "main-div">
      <img src="../Images/Philippines - Background.svg" alt = "background" style="position: absolute; z-index: -1; top: -500px; transform: rotate(-45deg);">
    </div>
    <div id = "about-div">
      <div id = "motto">
        <p id = group>COVID-19</p>
        <p id = "message">
          Clean your hands often. Use soap and water, or an alcohol-based hand rub.
          Maintain a safe distance from anyone who is coughing or sneezing.
          Don’t touch your eyes, nose or mouth.
          Cover your nose and mouth with your bent elbow or a tissue when you cough or sneeze.
          Stay home if you feel unwell.
          If you have a fever, a cough, and difficulty breathing, seek medical attention. Call in advance.
          Follow the directions of your local health authority.
        </p>
      </div>
      <div id = "team">
        <p id = group>Symptoms</p>
        <p id = "name">
          fever. tiredness. dry cough. aches and pains. nasal congestion. runny nose. sore throat. diarrhea.
          On average it takes 5–6 days from when someone is infected with the virus for symptoms to
          show, however it can take up to 14 days. People with mild symptoms who are otherwise healthy
          should self-isolate. Seek medical attention if you have a fever, a cough, and difficulty breathing.
          Call ahead.
        </p>
      </div>
      <div id = "team">
        <p id = group>Prevention</p>
        <p id = "name">
          Clean your hands often. Use soap and water, or an alcohol-based hand rub.
          Maintain a safe distance from anyone who is coughing or sneezing.
          Don’t touch your eyes, nose or mouth.
          Cover your nose and mouth with your bent elbow or a tissue when you cough or sneeze.
          Stay home if you feel unwell.
          If you have a fever, a cough, and difficulty breathing, seek medical attention. Call in advance.
          Follow the directions of your local health authority.
          <div style="height: 50px;"></div>
        </p>
      </div>
    </div>
  </main>
</body>
</html>
