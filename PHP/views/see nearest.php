<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Covaware</title>
  <link rel="stylesheet" href="./../css/See Nearest.css">
</head>
<body>
  <header>
    <div>
      <img src="../Images/Logo.svg" id = "logo" alt="logo" style="width: 141.74px; height: 38.11px; align-self: center;">
      <p id = "header-button" onclick="javascript:window.location='./../index.php'">Home</p>
    </div>
  </header>
  <main>
    <img src="../Images/Philippines - Background.svg" alt = "background" style="position: absolute; z-index: -1; top: -500px; transform: rotate(-45deg);">
    <div id = "scroll">
      <div id = "nearest-group">
        <div id = "nearest-box">
          <div style="width: 80%;">
            <p style="font-weight: 500; font-size: 18px; color: #ACACAC;">Nearest Hospital in your Area</p>
          </div>
          <div id = "line" style="height: 1px; width: 90%; background-color:#D5D5D5; align-self: center;"></div>
          <div style="width: 80%;">
            <p style="font-weight: 600; font-size: 23px; color: #272020;">4</p>
          </div>
        </div>
      </div>
      <div id = "hospital-group" class="hospital-group-2">
        <div id = "hospital-div">
          <div id = "image"></div>
          <div id = "text">
            <div id = "title">
              <p>Malolos National Hospital</p>
            </div>
            <div>
              <div id = "distance">
                <p>Distance - 5 mins</p>
              </div>
            </div>
          </div>
        </div>
        <div id = "hospital-div">
          <div id = "image"></div>
          <div id = "text">
            <div id = "title">
              <p>Malolos National Hospital</p>
            </div>
            <div>
              <div id = "distance">
                <p>Distance - 5 mins</p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id = "hospital-group" class="hospital-group-1">
        <div id = "hospital-div">
          <div id = "image"></div>
          <div id = "text">
            <div id = "title">
              <p>Malolos National Hospital</p>
            </div>
            <div>
              <div id = "distance">
                <p>Distance - 5 mins</p>
              </div>
            </div>
          </div>
        </div>
        <div id = "hospital-div">
          <div id = "image"></div>
          <div id = "text">
            <div id = "title">
              <p>Malolos National Hospital</p>
            </div>
            <div>
              <div id = "distance">
                <p>Distance - 5 mins</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</body>
</html>
