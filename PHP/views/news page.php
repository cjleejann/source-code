<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Covaware</title>
  <link rel="stylesheet" href="./../css/News Page.css">
</head>
<body>
  <header>
    <div>
      <img src="../Images/Logo.svg" id = "logo" alt="logo" style="width: 141.74px; height: 38.11px; align-self: center;">
      <p id = "header-button" onclick="javascript:window.location='./../index.php'">Home</p>
    </div>
  </header>
  <main>
    <img src="../Images/Philippines - Background.svg" alt = "background" style="position: absolute; z-index: -1; top: -500px; transform: rotate(-45deg); ">
    <div id = "news-group">
      <div id = "news-image"></div>
      <div id = "title">
        <p>Publisher</p>
      </div>
      <div id = "publisher">
        <p>Malacañang - April 29, 2020</p>
      </div>
      <div id = "message">
        Metro Manila (CNN Philippines, April 29) — The total number of coronavirus disease cases in the country reached 8,212 on Wednesday, with more than a thousand surviving from the viral illness.

        The Department of Health confirmed 254 new cases, adding that 48 more people have recovered from COVID-19, bringing the total number of recoveries to 1,023.

        Meanwhile, 28 more patients died of the disease. The nationwide death toll is now at 558.

        The DOH has explained these are not the real-time data since cases have to be validated before reporting.

        In an interview with CNN Philippines, Health Spokesperson Maria Rosario Vergeire said 62 percent of the country's 6,631 active cases exhibit mild symptoms. It takes around 13 days up to more than a month for patients to recover, depending on severity of symptoms.

        The country can now test up to 6,420 samples daily, Vergeire said in an online briefing, while admitting that it may not meet by Thursday its target to conduct 8,000 tests per day. She said the agency will continue to work to expand the country's testing capacity.

        Vergeire said a total of 89,021 coronavirus tests have been conducted since January, when the Philippines reported its first COVID-19 case.

        Metro Manila and several provinces are under enhanced community quarantine, marked by stay-at-home orders and suspension of most work and mass transportation, to contain the spread of COVID-19.

        Worldwide, COVID-19 has infected more than 3.1 million people and killed over 217,000 since the outbreak began in Wuhan, China in December 2019.
      </div>
    </div>
  </main>
</body>
</html>
