<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Covaware</title>
  <link rel="stylesheet" href="./../css/Contact.css">
</head>
<body>
  <header>
    <div>
      <img src="../Images/Logo.svg" id = "logo" alt="logo" style="width: 141.74px; height: 38.11px; align-self: center;">
      <p id = "header-button" onclick="javascript:window.location='./../index.php'">Home</p>
    </div>
  </header>
  <main>
    <img src="../Images/Philippines - Background.svg" alt = "background" style="position: absolute; z-index: -1; top: -500px; transform: rotate(-45deg); ">
    <div id = "main-div">
      <div id = "left-div">
        <div id ="nearest-div">
          <div id = "nearest-image"></div>
          <div id = "nearest-title">
            <p>Hospital nearest to you</p>
          </div>
          <div id = "line" style="height: 1px; width: 90%; background-color:#D5D5D5; margin-bottom: -25px;"></div>
          <div id = "hospital-title">
            <p>Malolos National Hospital</p>
          </div>
        </div>
        <div id = "textarea-div">
          <div id = "nearest-title" style="margin-top: -20px;">
            <p>Send Message</p>
          </div>
          <div id = "line" style="height: 1px; width: 90%; background-color:#D5D5D5;"></div>
          <textarea id = "textarea-txtar" placeholder="Enter Message Here.."></textarea>
          <div style="background-color: #00B9FF; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
            <p style="font-weight:400; color:white; font-size: 15px; margin: auto;">Send Message</p>
          </div>
        </div>
      </div>
      <div id ="right-div">
        <div id ="address-div">
          <div id = "hospital-address-title">
            <p>Malolos National Hospital</p>
            <p>Information</p>
          </div>
          <div id = "line" style="height: 1px; width: 90%; background-color:#D5D5D5;"></div>
          <div id = "address-block">
            <p id = "title">Address</p>
            <p id = "Info">15 Paseo del Congreso St, Malolos, 3000 Bulacan</p>
          </div>
          <div id = "address-block">
            <p id = "title">Contact Number</p>
            <p id = "Info">+63 945 133 8110	</p>
          </div>
          <div id = "address-block">
            <p id = "title">Distance</p>
            <p id = "Info">0.2 KM Away from your location (5 mins)</p>
          </div>
        </div>
        <div id ="contact-div">
          <div id ="contact-box">
            <div id = "hospital-address-title" style="margin-top: -20px;">
              <p>Alert Hospital of your location?</p>
            </div>
            <div id = "line" style="height: 1px; width: 90%; background-color:#D5D5D5; margin-top: -15px;"></div>
            <div style="background-color: #FF6E6E; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
              <p style="font-weight:400; color:white; font-size: 15px; margin: auto;">Yes</p>
            </div>
          </div>
          <div id ="contact-box">
            <div id = "hospital-address-title" style="margin-top: -20px;">
              <p>Alert Police of your location?</p>
            </div>
            <div id = "line" style="height: 1px; width: 90%; background-color:#D5D5D5; margin-top: -15px;"></div>
            <div style="background-color: #FF6E6E; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
              <p style="font-weight:400; color:white; font-size: 15px; margin: auto;">Yes</p>
            </div>
          </div>
          <div id ="contact-box">
            <div id = "hospital-address-title" style="margin-top: -20px;">
              <p>Alert LGU of your location?</p>
            </div>
            <div id = "line" style="height: 1px; width: 90%; background-color:#D5D5D5; margin-top: -15px;"></div>
            <div style="background-color: #FF6E6E; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
              <p style="font-weight:400; color:white; font-size: 15px; margin: auto;">Yes</p>
            </div>
          </div>
        </div>
        <div id="footer-div"></div>
      </div>
    </div>
  </main>
</body>
</html>
