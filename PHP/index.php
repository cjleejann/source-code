<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Covaware</title>
  <link rel="stylesheet" href="css/Home.css">
</head>
<body>
  <?php include 'public/connection.php' ?>
  <header>
    <div>
      <img src="Images/Logo.svg" id = "logo" alt="logo" style="width: 141.74px; height: 38.11px; align-self: center;">
      <p id = "header-button" onclick="javascript:window.location='views/about.php'">About</p>
    </div>
  </header>
  <main>
    <div id = "main-div">
      <img src="Images/Philippines - Background.svg" alt = "background" style="position: absolute; z-index: -1; top: -500px; transform: rotate(-45deg);">
      <div id = "main-map-div">
        <img src="Images/Red-Circle.svg" alt = "target" style="position: absolute; width: 9px; height: 9px; top: 229px; left: 203px; box-shadow: 0px 3px 5px rgb(0, 0, 0, 0.1); border-radius:100%;" id="circle" onmouseover="on_hover(this)" onmouseout="on_out()">
        <img src="Images/Red-Circle.svg" alt = "target" style="position: absolute; width: 14px; height: 14px; top: 369px; left: 203px; box-shadow: 0px 3px 5px rgb(0, 0, 0, 0.1); border-radius:100%;" id="circle" onmouseover="on_hover(this)" onmouseout="on_out()">
        <img src="Images/Red-Circle.svg" alt = "target" style="position: absolute; width: 21px; height: 21px; top: 339px; left: 200px; box-shadow: 0px 3px 5px rgb(0, 0, 0, 0.1); border-radius:100%;" id="circle" onmouseover="on_hover(this)" onmouseout="on_out()">
        <img src="Images/Red-Circle.svg" alt = "target" style="position: absolute; width: 17px; height: 17px; top: 299px; left: 190px; box-shadow: 0px 3px 5px rgb(0, 0, 0, 0.1); border-radius:100%;" id="circle" onmouseover="on_hover(this)" onmouseout="on_out()">
        <img src="Images/Red-Circle.svg" alt = "target" style="position: absolute; width: 10px; height: 10px; top: 559px; left: 350px; box-shadow: 0px 3px 5px rgb(0, 0, 0, 0.1); border-radius:100%;" id="circle" onmouseover="on_hover(this)" onmouseout="on_out()">
        <img src="Images/Red-Circle.svg" alt = "target" style="position: absolute; width: 10px; height: 10px; top: 729px; left: 430px; box-shadow: 0px 3px 5px rgb(0, 0, 0, 0.1); border-radius:100%;" id="circle" onmouseover="on_hover(this)" onmouseout="on_out()">
        <div id ="pop-up">
          <p style="font-size: 12px; font-weight: 500; text-align: center;"><?php echo $cityMetroManila ?></p>
          <p style="font-size: 18px; font-weight: 600; margin-top: -10px; text-align: center;"><?php echo $caseMetroManila ?></p>
          <p style="font-size: 13px; font-weight: 500; margin-top: -20px; text-align: center;">cases</p>
        </div>
        <img src="Images/Philippines - Map.svg" alt = "map" id = "ph-map">
      </div>
      <div id = "box-div">
        <div id = "middle-div">
          <div id = "total-div">
            <div id = "group">
              <p id = "title">Total Cases in the Country</p>
              <div id = "line" style="height: 1px; width: 100%; background-color:#D5D5D5;"></div>
              <p id = "value"><?php echo $caseMetroManila ?></p>
            </div>
          </div>
          <div id = "list-div">
            <div id = "group">
              <div id ="title-group" style="display:flex; justify-content: space-between; width: 93%;">
                <p id = "title">Areas</p>
                <p id = "sub-title">Cases</p>
              </div>
              <div id = "line" style="height: 1px; width: 100%; background-color:#D5D5D5;"></div>
              <ul style="list-style-type:none; overflow-y: scroll; height:170px;">
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityManila ?></li>
                  <li id = "case-li"><?php echo $caseManila ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityMalolos ?></li>
                  <li id = "case-li"><?php echo $caseMalolos ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityMakati ?></li>
                  <li id = "case-li"><?php echo $caseMakati ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityTaguig ?></li>
                  <li id = "case-li"><?php echo $caseTaguig ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityPasig ?></li>
                  <li id = "case-li"><?php echo $casePasig ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityQuezon ?></li>
                  <li id = "case-li"><?php echo $caseQuezon ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityPateros ?></li>
                  <li id = "case-li"><?php echo $casePateros ?></li>
                </span>
              </ul>
            </div>
          </div>
          <div id = "list-div">
            <div id = "group">
              <div id ="title-group" style="display:flex; justify-content: space-between; width: 93%;">
                <p id = "title">Areas</p>
                <p id = "sub-title">Cases</p>
              </div>
              <div id = "line" style="height: 1px; width: 100%; background-color:#D5D5D5;"></div>
              <ul style="list-style-type:none; overflow-y: scroll; height:170px;">
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityManila ?></li>
                  <li id = "case-li"><?php echo $caseManila ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityMalolos ?></li>
                  <li id = "case-li"><?php echo $caseMalolos ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityMakati ?></li>
                  <li id = "case-li"><?php echo $caseMakati ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityTaguig ?></li>
                  <li id = "case-li"><?php echo $caseTaguig ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityPasig ?></li>
                  <li id = "case-li"><?php echo $casePasig ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityQuezon ?></li>
                  <li id = "case-li"><?php echo $caseQuezon ?></li>
                </span>
                <span style="display: flex; width: 105%; justify-content: space-between; margin-left: -40px;" id = "span-li">
                  <li id = "area-li"><?php echo $cityPateros ?></li>
                  <li id = "case-li"><?php echo $casePateros ?></li>
                </span>
              </ul>
            </div>
          </div>
        </div>
        <div id = "right-div">
          <div id = "help-div">
            <div style="width: 90%;">
              <p id = "title">Need Help?</p>
            </div>
            <div id = "button-group">
              <div id = "button-break">
                <div style="background-color: #00B9FF; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
                  <p style="font-weight:400; color:white; font-size: 15px; margin: auto;" onclick="javascript:window.location='views/contact.php'">Contact LGU</p>
                </div>
                <div style="background-color: #00B9FF; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
                  <p style="font-weight:400; color:white; font-size: 15px; margin: auto;" onclick="javascript:window.location='views/contact.php'">Contact Police</p>
                </div>
              </div>
              <div id = "button-break">
                <div style="background-color: #00B9FF; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
                  <p style="font-weight:400; color:white; font-size: 15px; margin: auto;" onclick="javascript:window.location='views/see nearest.php'">See Nearest Hospital</p>
                </div>
                <div style="background-color: #00B9FF; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
                  <p style="font-weight:400; color:white; font-size: 15px; margin: auto;" onclick="javascript:window.location='views/contact.php'">Contact Nearest Hospital</p>
                </div>
              </div>
              <div id = "button-break">
                <div style="background-color: #00B9FF; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
                  <p style="font-weight:400; color:white; font-size: 15px; margin: auto;" onclick="javascript:window.location='views/covid-19.php'">About COVID-19</p>
                </div>
                <div style="background-color: #00B9FF; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
                  <p style="font-weight:400; color:white; font-size: 15px; margin: auto;" onclick="javascript:window.location='views/announcements.php'">Announcement</p>
                </div>
              </div>
            </div>
          </div>
          <div id = "news-div">
            <div id = "title-group">
              <p>Latest News</p>
            </div>
            <div id = "image-news-div"></div>
            <div id = "news-title-group">
              <p>COVID recoveries passes death toll </p>
            </div>
            <div id = "publisher-title-group">
              <p id = "publisher-title">By Philstar </p>
              <p id = "publisher-date" style="font-size:14px;">April 26, 2020</p>
            </div>
            <div style="background-color: #00B9FF; border-radius: 7px; box-shadow: 0px 5px 10px rgb(49, 199, 255, 0.1); width: 200px; height: 52px; display:flex; flex-direction: column; align-items: center; cursor: pointer;">
              <p style="font-weight:400; color:white; font-size: 15px; margin: auto;" onclick="javascript:window.location='views/news.php'">See News</p>
            </div>
          </div>
        </div>
        <div id = "div-footer"></div>
      </div>
    </div>
  </main>
  <footer>

  </footer>
  <script src="js/main.js" charset="utf-8"></script>
</body>
</html>
