<?php
$servername = "localhost";
$username = "root";
$password = "12345";
$db = "dbcovaware";

// Create connection
$conn = new mysqli($servername, $username, $password, $db);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$metroManila = "SELECT City FROM tblphilippines WHERE City='Metro Manila'";
$sMetroManila = $conn->query($metroManila);
if ($sMetroManila->num_rows > 0) {
  while($row = $sMetroManila->fetch_assoc()) {
    $cityMetroManila = $row['City'];
  }
}

$manila = "SELECT City FROM tblphilippines WHERE City='Manila'";
$sManila = $conn->query($manila);
if ($sManila->num_rows > 0) {
  while($row = $sManila->fetch_assoc()) {
    $cityManila = $row['City'];
  }
}

$malolos = "SELECT City FROM tblphilippines WHERE City='Malolos'";
$sMalolos = $conn->query($malolos);
if ($sMalolos->num_rows > 0) {
  while($row = $sMalolos->fetch_assoc()) {
    $cityMalolos = $row['City'];
  }
}

$makati = "SELECT City FROM tblphilippines WHERE City='Makati'";
$sMakati = $conn->query($makati);
if ($sMakati->num_rows > 0) {
  while($row = $sMakati->fetch_assoc()) {
    $cityMakati = $row['City'];
  }
}

$taguig = "SELECT City FROM tblphilippines WHERE City='Taguig'";
$sTaguig = $conn->query($taguig);
if ($sTaguig->num_rows > 0) {
  while($row = $sTaguig->fetch_assoc()) {
    $cityTaguig = $row['City'];
  }
}

$pasig = "SELECT City FROM tblphilippines WHERE City='Pasig'";
$sPasig = $conn->query($pasig);
if ($sPasig->num_rows > 0) {
  while($row = $sPasig->fetch_assoc()) {
    $cityPasig = $row['City'];
  }
}

$quezon = "SELECT City FROM tblphilippines WHERE City='Quezon'";
$sQuezon = $conn->query($quezon);
if ($sQuezon->num_rows > 0) {
  while($row = $sQuezon->fetch_assoc()) {
    $cityQuezon = $row['City'];
  }
}

$pateros = "SELECT City FROM tblphilippines WHERE City='Pateros'";
$sPateros = $conn->query($pateros);
if ($sPateros->num_rows > 0) {
  while($row = $sPateros->fetch_assoc()) {
    $cityPateros = $row['City'];
  }
}

/////////////////////////////////////////////////////////////////////

$cmetroManila = "SELECT Cases FROM tblphilippines WHERE City='Metro Manila'";
$csMetroManila = $conn->query($cmetroManila);
if ($csMetroManila->num_rows > 0) {
  while($row = $csMetroManila->fetch_assoc()) {
    $caseMetroManila = $row['Cases'];
  }
}

$cmanila = "SELECT Cases FROM tblphilippines WHERE City='Manila'";
$csManila = $conn->query($cmanila);
if ($csManila->num_rows > 0) {
  while($row = $csManila->fetch_assoc()) {
    $caseManila = $row['Cases'];
  }
}

$cmalolos = "SELECT Cases FROM tblphilippines WHERE City='Malolos'";
$csMalolos = $conn->query($cmalolos);
if ($csMalolos->num_rows > 0) {
  while($row = $csMalolos->fetch_assoc()) {
    $caseMalolos = $row['Cases'];
  }
}

$cmakati = "SELECT Cases FROM tblphilippines WHERE City='Makati'";
$csMakati = $conn->query($cmakati);
if ($csMakati->num_rows > 0) {
  while($row = $csMakati->fetch_assoc()) {
    $caseMakati = $row['Cases'];
  }
}

$ctaguig = "SELECT Cases FROM tblphilippines WHERE City='Taguig'";
$csTaguig = $conn->query($ctaguig);
if ($csTaguig->num_rows > 0) {
  while($row = $csTaguig->fetch_assoc()) {
    $caseTaguig = $row['Cases'];
  }
}

$cpasig = "SELECT Cases FROM tblphilippines WHERE City='Pasig'";
$csPasig = $conn->query($cpasig);
if ($csPasig->num_rows > 0) {
  while($row = $csPasig->fetch_assoc()) {
    $casePasig = $row['Cases'];
  }
}

$cquezon = "SELECT Cases FROM tblphilippines WHERE City='Quezon'";
$csQuezon = $conn->query($cquezon);
if ($csQuezon->num_rows > 0) {
  while($row = $csQuezon->fetch_assoc()) {
    $caseQuezon = $row['Cases'];
  }
}

$cpateros = "SELECT Cases FROM tblphilippines WHERE City='Pateros'";
$csPateros = $conn->query($cpateros);
if ($csPateros->num_rows > 0) {
  while($row = $csPateros->fetch_assoc()) {
    $casePateros = $row['Cases'];
  }
}

?>
